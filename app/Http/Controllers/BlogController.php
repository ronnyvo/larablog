<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use App\Repositories\PostRepository;
//use App\Repositories\UserRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{

    const perPage = 5;

    protected $postRepo;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepo = $postRepository;
    }

    public function index() {

        $posts = Post::where('publish', true)->orderBy('created_at', 'desc')->simplePaginate($this::perPage);

        return view('blog.index', ['posts' => $posts]);

    }

    public function createPost() {
        return view('blog.create');
    }

    public function storePost(Request $request) {

        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required',
        ]);

        $data = $request->all();

        $post = $this->postRepo->createFromRequest($data);

        return redirect()->route('blog')->with('status', 'Your post is created successfully!');
    }

    public function editPost(Post $post) {

        $this->authorize('update', $post);

        return view('blog.edit', ['post' => $post]);
    }

    public function updatePost(Post $post, Request $request) {

        $this->authorize('update', $post);

        $data = $request->all();

        $post = $this->postRepo->updateFromRequest($post, $data);

        return redirect()->route('blog');
    }

    public function viewPost(Post $post) {

        $comments = $post->comments()->orderBy('created_at', 'desc')->paginate($this::perPage);

        $featured_image = '/images/post-bg.jpg';
        if ($post->featured_image) {
            $featured_image = asset('storage/').'/'.$post->featured_image;
        }

        return view('blog.single', ['post' => $post, 'featured_image' => $featured_image, 'comments' => $comments]);
    }

    public function confirmDeletePost(Post $post) {

        $this->authorize('delete', $post);

        return view('blog.confirm_delete', ['post' => $post]);
    }

    public function deletePost(Post $post) {

        $post->delete();

        return redirect('blog');
    }

    public function storeComment(Request $request, Post $post) {

        $this->validate($request, [
            'comment_body' => 'required'
        ]);

        $comment = new Comment();
        $comment->content = $request->input('comment_body');;
        $comment->post_id = $post->id;
        $comment->user_id = Auth::user()->id;
        $comment->save();
        $redirect_url = route('post-view', $post).'#comment-list';
        return redirect($redirect_url);
    }

    public function redirectToCommentForm(Post $post) {
        $redirect_url = route('post-view', $post).'#comment-form';
        return redirect($redirect_url);
    }
}

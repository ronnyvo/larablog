<?php

namespace App\Http\Controllers;

use App\Mail\ConfirmContactUs;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    public function search(Request $request) {

        $key = null;

        $posts = collect();
        if ($request->has('key')) {
            $key = $request->input('key');
            $posts = Post::search($key)->get();
        }

        return view('page.search', ['posts'=> $posts, 'key' => $key]);
    }

    public function viewContactPage() {
        return view('page.contact');
    }

    public function viewAboutPage() {
        return view('page.about');
    }

    public function submitContactForm(Request $request) {
        $data = $request->all();
        Mail::to($data['email'])->send(new ConfirmContactUs($data));
        return response('Mail sent', 200)
            ->header('Content-Type', 'text/plain');
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class RedirectTo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('redirect_to')) {
            $request->session()->put('app_redirect_to', $request->input('redirect_to'));
        }else {
            $redirect_to = $request->session()->get('app_redirect_to');
            if ($redirect_to) {
                $request->session()->forget('app_redirect_to');
                return response()->redirectTo($redirect_to);
            }
        }
        return $next($request);
    }
}

<?php
namespace App\Repositories;

use App\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

Class PostRepository
{
    public function createFromRequest($data) {

        $data['publish'] = isset($data['publish']) ? true : false;
        $data['user_id'] = Auth::user()->id;

        $post = new Post();
        $post->title = $data['title'];
        $post->summary = $data['summary'];
        $post->description = $data['description'];
        $post->publish = $data['publish'];
        $post->user_id = $data['user_id'];

        if (isset($data['featured_image']) && $data['featured_image']->isValid()) {
            $path = $data['featured_image']->store('images','public');
            $post->featured_image = $path;
        }

        $post->save();

        return $post;
    }

    public function updateFromRequest($post, $data) {

        $data['publish'] = isset($data['publish']) ? true : false;

        $post->title = $data['title'];
        $post->summary = $data['summary'];
        $post->description = $data['description'];
        $post->publish = $data['publish'];
        //$post->user_id = $data['user_id'];

        if (isset($data['featured_image']) && $data['featured_image']->isValid()) {
            $path = $data['featured_image']->store('images','public');
            $post->featured_image = $path;
        }elseif ($post->featured_image) {
            Storage::delete($post->featured_image);
            $post->featured_image = null;
        }

        $post->save();

        return $post;
    }
}
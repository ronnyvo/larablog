<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            'name' => 'admin',
            'email' => 'admin@mysite.com',
            'password' => bcrypt('123456'),
            'is_admin' => true
        ));
    }
}

jQuery(document).ready(function () {
   $('.delete-image').on('click', function (e) {
       e.preventDefault();
       var currentEl = $(this);
       currentEl.closest('.field-image-upload').find('img').remove();
       currentEl.remove();
       $('.field-image-upload').append('<input type="file" id="featured_image" name="featured_image">');
   }) 
});
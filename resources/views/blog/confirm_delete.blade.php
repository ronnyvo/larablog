@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('post-delete', $post) }}">
                            {{ csrf_field() }}
                            {{ method_field('delete') }}
                            <div class="form-group">
                                <p class="confirmation">Are you sure you want to delete this post?</p>
                                <div class="col-md-12">
                                    <a href="#" onclick="window.history.back()" class="btn btn-default">
                                        Cancel
                                    </a>
                                    <button type="submit" class="btn btn-danger">
                                        Delete
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

            </div>
        </div>
    </div>
@endsection

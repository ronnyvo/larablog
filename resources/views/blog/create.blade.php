@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create a post</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('post-store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title" class="col-md-2 control-label">Title</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="title"  id="title" placeholder="Title">
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="featured_image" class="col-md-2 control-label">Featured image</label>
                                <div class="col-md-10">
                                    <input type="file" id="featured_image" name="featured_image">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="summary" class="col-md-2 control-label">Summary</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" name="summary" id="summary" placeholder="Summary"></textarea>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="body" class="col-md-2 control-label">Body</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" name="description" id="body" placeholder="Body" rows="5"></textarea>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="publish" class="col-md-2 control-label">Publish</label>
                                <div class="col-md-8 col-md-offset-1">
                                    <div class="checkbox">
                                        <input type="checkbox" id="publish" name="publish">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary">
                                        Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'body' );
    </script>
    <script src="{{ asset('static') }}/js/vendor/jquery.ui.widget.js"></script>
    <script src="{{ asset('static') }}/js/jquery.iframe-transport.js"></script>
    <script src="{{ asset('static') }}/js/jquery.fileupload.js"></script>
    <script>
        $(function () {
//            $('#fileupload').fileupload({
//                dataType: 'json',
//                done: function (e, data) {
//                    $.each(data.result.files, function (index, file) {
//                        $('<p/>').text(file.name).appendTo(document.body);
//                    });
//                }
//            });
        });
    </script>
@endpush
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit &quot;{{ $post->title }}&quot;</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('post-update',[$post]) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="title" class="col-md-2 control-label">Title</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="title"  id="title" placeholder="Title" value="{{ $post->title }}">
                                </div>
                            </div>

                            <div class="form-group field-image">
                                <label for="featured_image" class="col-md-2 control-label">Featured image</label>
                                <div class="col-md-10 field-image-upload">
                                    @if ($post->featured_image)
                                        <img src="{{ asset('storage').'/'.$post->featured_image }}" style="height: 100px; width: auto">
                                        <a href="#" class="delete-image">Delete</a>
                                    @else
                                        <input type="file" id="featured_image" name="featured_image">
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="summary" class="col-md-2 control-label">Summary</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" name="summary" id="summary" placeholder="Summary">{{ $post->summary }}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="body" class="col-md-2 control-label">Body</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" name="description" id="body" placeholder="Body" rows="5">{{ $post->description }}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="publish" class="col-md-2 control-label">Publish</label>
                                <div class="col-md-8 col-md-offset-1">
                                    <div class="checkbox">
                                        <input type="checkbox" id="publish" name="publish" @if ($post->publish) {{ 'checked' }} @endif>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary">
                                        Save changes
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'body' );
    </script>
@endpush

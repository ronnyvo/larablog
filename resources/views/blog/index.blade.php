@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('status'))
            <div class="row">
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            </div>
        @endif
        <div class="row">
            <p>To begin to create a post, click this link <a href="{{ route('post-create') }}" title="Create a post">Create a post</a>.</p>
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                @each('blog.partials.post_index', $posts, 'post', 'blog.partials.post_index_empty')

                {{--Display pagination--}}
                @if (!$posts->isEmpty())
                    {{ $posts->links() }}
                @endif
            </div>
        </div>
    </div>
@endsection

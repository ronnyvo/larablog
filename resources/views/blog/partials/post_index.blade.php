<div class="post-preview">
    <a href="{{ route('post-view', $post) }}">
        <h2 class="post-title">
            {{ $post->title }}
        </h2>
        <h3 class="post-subtitle">
            {{ $post->summary }}
        </h3>
    </a>
    <p class="post-meta">Posted by <a href="#">{{ $post->user->name }}</a> on {{ $post->day_published }}</p>
    <div class="actions">
    @can('update', $post)
        <!-- The Current User Can Update The Post -->
            <a href="{{ route('post-edit', $post) }}" title="Edit post">Edit</a>
    @endcan
    @can('delete', $post)
        <!-- The Current User Can Delete The Post -->
            <a href="{{ route('post-confirm-delete', $post) }}" title="Edit post">Delete</a>
        @endcan
    </div>
</div>
<hr>
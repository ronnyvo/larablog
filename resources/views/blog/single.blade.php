@extends('layouts.app')

@section('header-class','post-heading')
@section('header-image', $featured_image)
@section('header-title',$post->title)
@section('header-subtitle',$post->summary)
@section('header-meta')
    <span class="meta">Posted by <a href="#">{{ $post->user->name }}</a> on {{ $post->day_published }}</span>
@endsection

@section('content')
    <article>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    {{--{!! nl2br(e($post->description)) !!}--}}
                    {!! $post->description !!}
                </div>
            </div>
        </div>
    </article>
    <div class="comment-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <h3>Post a comment</h3>
                    @if (Auth::check())
                        <form action="{{ route('comment-store', $post) }}" method="post" id="comment-form">

                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('comment_body') ? ' has-error' : '' }}">
                                <textarea class="form-control" name="comment_body" placeholder="Post a comment..."></textarea>
                                @if ($errors->has('comment_body'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('comment_body') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Post a comment</button>
                            </div>


                        </form>
                    @else
                        <p>To comment on this post, please <a href="{{ route('comment-redirect', $post) }}" title="login">login</a>.</p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    @include('comments.list', ['comments' => $comments])
                </div>
            </div>
        </div>
    </div>
@endsection

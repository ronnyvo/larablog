<div class="comment-item">
    <div class="row">
        <div class="col-md-2">
            <p><a href="#" class="comment-author">{{ $comment->user->name }}</a></p>
        </div>
        <div class="col-md-10">
            <p>{!! nl2br(e($comment->content)) !!}</p>
        </div>
    </div>
</div>
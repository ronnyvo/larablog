<div class="comments">
    <h3 id="comment-list">Comments @if(!$comments->isEmpty()){{ '('.$comments->total().')' }}@endif</h3>
    @each('comments.item', $comments, 'comment', 'comments.item_empty')
    <div class="comments-pager">
        @if (!$comments->isEmpty())
            {{ $comments->links() }}
        @endif
    </div>
</div>
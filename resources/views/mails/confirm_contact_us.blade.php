<p>Hello, {{ $data['name'] }}</p>
<p>Thanks for contact us. Here is your submitted information:</p>
<ul>
    <li>Name: {{ $data['name'] }}</li>
    <li>Email: {{ $data['email'] }}</li>
    <li>Phone: {{ $data['phone']}}</li>
    <li>Message: {{ $data['message'] }}</li>
</ul>
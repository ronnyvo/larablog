@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <form method="get" action="{{ url('search') }}">
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-2">
                        <input class="form-control input-lg" type="text" name="key" placeholder="Search here..." value="{{ $key }}">
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="row">
            @if (!$posts->isEmpty())
                @each('page.search_item',$posts,'post')
            @elseif(isset($key))
                <div class="no-result">
                    <div class="col-md-12">
                        <p>No results found.</p>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

<div class="col-md-12">
    <div class="row">
        <div class="col-md-8">
            <h3><a href="{{ route('post-view', $post) }}" title="View more">{{ $post->title }}</a></h3>
            <span class="meta">{{ $post->user->name }} | {{ $post->day_published }}</span>
            <p class="summary">{{ $post->summary }}</p>
        </div>
        <div class="col-md-4">
            <div class="actions">
            @can('update', $post)
                <!-- The Current User Can Update The Post -->
                    <a href="{{ route('post-edit', $post) }}" title="Edit post">Edit</a>
            @endcan
            @can('delete', $post)
                <!-- The Current User Can Delete The Post -->
                    <a href="{{ route('post-confirm-delete', $post) }}" title="Edit post">Delete</a>
                @endcan
            </div>
        </div>
    </div>
</div>
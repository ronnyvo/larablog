<ul class="pager">
    <!-- Previous Page Link -->
    @if ($paginator->onFirstPage())
    @else
        <li class="previous"><a href="{{ $paginator->previousPageUrl() }}" rel="prev">Newer Posts &laquo;</a></li>
    @endif

    <!-- Next Page Link -->
    @if ($paginator->hasMorePages())
        <li class="next"><a href="{{ $paginator->nextPageUrl() }}" rel="next">Older Posts &raquo;</a></li>
    @else
    @endif
</ul>
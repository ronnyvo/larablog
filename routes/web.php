<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return redirect('blog');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/about', 'HomeController@viewAboutPage');

//Blog routing
Route::group(['prefix' => 'blog'], function () {

    Route::get('/', 'BlogController@index')->name('blog');

    Route::group(['middleware' => 'auth'], function () {

        Route::get('/create', 'BlogController@createPost')->name('post-create');

        Route::post('/store', 'BlogController@storePost')->name('post-store');

        Route::get('/{post}/edit', 'BlogController@editPost')->name('post-edit');

        Route::post('/{post}/update', 'BlogController@updatePost')->name('post-update');

        Route::get('/{post}/delete', 'BlogController@confirmDeletePost')->name('post-confirm-delete');
        
        Route::delete('/{post}', 'BlogController@deletePost')->name('post-delete');

        Route::post('/{post}/comment', 'BlogController@storeComment')->name('comment-store');

        Route::get('/{post}/post-comment', 'BlogController@redirectToCommentForm')->name('comment-redirect');

    });

    Route::get('/{post}', 'BlogController@viewPost')->name('post-view');
});
//
//Route::get('/search', function (\Illuminate\Http\Request $request) {
//    return \App\Post::search($request->search)->get();
//});

Route::get('/search', 'HomeController@search');

Route::get('/contact-us', 'HomeController@viewContactPage')->name('contact-view');

Route::post('/contact-us', 'HomeController@submitContactForm')->name('contact-submit');

Route::group(['prefix' => 'admin', 'middleware' => ['auth','is_admin']], function () {
    Route::get('/',function (){
       return 'If you get here, you are really a very handsome guy.';
    });
    Route::get('/search/reindex', 'AdminController@searchReindex')->name('search-reindex');
});